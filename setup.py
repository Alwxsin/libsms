from setuptools import setup, find_packages

setup(
    name="django-libsms",
    version='0.0.1',
    url='https://bitbucket.org/Alwxsin/libsms',
    license='BSD',
    platforms=['OS Independent'],
    description="A simple API to send SMS messages.",
    long_description=open('README.rst').read(),
    author='Alexandr Sinichkin',
    author_email='alwxsin@gmail.com',
    maintainer='Alexandr Sinichkin',
    maintainer_email='alwxsin@gmail.com',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    classifiers=[
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP',
    ]
)
