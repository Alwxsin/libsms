class BaseSmsTransport(object):
    """
    Base class for sms backend implementations.

    Subclasses must at least overwrite send()
    """

    def send(self, phone=None, msg=None):
        """
        Sends SmsMessage object and prints the phone number and message
        """
        raise NotImplementedError
