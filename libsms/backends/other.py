from libsms.backends.base import BaseSmsTransport
from django.core.exceptions import ImproperlyConfigured


class SmsTransport(BaseSmsTransport):
    def __init__(self, params=None):
        self.login = params['login'] if 'login' in params else ''
        self.password = params['password'] if 'password' in params else ''
        if not self.password or not self.login:
            raise ImproperlyConfigured(u'Error configuring other sms transport. Login or password are missing.')
        self.var1 = params['var1'] if 'var1' in params else ''
        self.var2 = params['var2'] if 'var2' in params else ''

    def send(self, phone=None, msg=None):
        print('Phone - ' + str(phone))
        print('Message - ' + str(msg))
