from . import get_connection


def send(phone=None, msg=None):
    result = get_connection().send(phone=phone, msg=msg)
    return result
