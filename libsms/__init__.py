from django.conf import settings
from django.core.exceptions import ImproperlyConfigured

try:
    # Django versions >= 1.9
    from django.utils.module_loading import import_module
except ImportError:
    # Django versions < 1.9
    from django.utils.importlib import import_module

transports = getattr(settings, 'SMS_TRANSPORTS', '')


def get_connection(name=None):
    """
    Load an sms backend and return an instance of it.

    :param string name: backend python name.
    :returns: backend class instance.
    :rtype: :py:class:`~libsms.backends.base.BaseSmsTransport` subclass
    """

    transport = transports.get(name) or transports.get('default', '')
    if not transport:
        raise ImproperlyConfigured(u'Error. Check SMS_TRANSPORT setting. Is there default option?')
    path = transport.get('BACKEND')

    mod_name, klass_name = path.rsplit('.', 1)
    try:
        module = import_module(mod_name)
    except AttributeError as e:
        raise ImproperlyConfigured(u'Error importing sms backend module %s: "%s"' % (mod_name, e))

    try:
        klass = getattr(module, klass_name)
    except AttributeError:
        raise ImproperlyConfigured(u'Module "%s" does not define a "%s" class' % (mod_name, klass_name))

    params = transport.get('PARAMS') if transport else None
    return klass(params=params)

sms_transports = {name: get_connection(name) for name in transports}
