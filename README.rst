=============
django-libsms
=============


A simple api to send SMS messages with django. The api is structured the same way as django's own email api.

Installation
============

::

    pip install git+https://bitbucket.org/Alwxsin/libsms

Configure the ``SMS_TRANSPORTS``::

    SMS_TRANSPORTS = {
    'default': {
        'BACKEND': 'libsms.backends.sms.SmsTransport',
        'PARAMS': {
            'login': 'some_login',
            'password': 'some_password',
        }
    },
    'dummy': {
        'BACKEND': 'libsms.backends.dummy.SmsTransport',
    },
    'other': {
        'BACKEND': 'libsms.backends.other.SmsTransport',
        'PARAMS': {
            'login': 'some_login',
            'password': 'some_password',
            'var1': 'var1',
            'var2': 'var2',
        }
    }
}


Basic usage
===========

Sending SMSs is like sending emails::

    from libsms import sms_transport
	sms_transport.send(phone=’123123’, msg=’qweqwe’)

you can also choose backend::

    from libsms import sms_transports
	sms_transports[‘dummy’].send(phone=’123123’, msg=’qweqwe’)



Custom backends
===============

Creating custom ``SmsBackend`` s::

    from libsms.backends.base import BaseSmsBackend
    from some.sms.delivery.api

    class AwesomeSmsBackend(BaseSmsBackend):
        def send(self, phone=None, msg=None):
            try:
                some.sms.delivery.api.send(
                    message=msg.body,
                    from_phone=msg.from_phone,
                    to_phone=phone
                )
            except:
                raise

Then all you need to do is reference your backend in the ``SMS_TRANSPORTS`` setting.

